package spider.mathserver.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import spider.mathserver.model.Category;
import spider.mathserver.service.CategoryService;

/**
 * Handles requests for the application home page.
 */
@RestController
@RequestMapping("/category")
public class CategoryController {

	@Autowired(required = true)
	private CategoryService categoryService;

	
	@RequestMapping(value = "/getall", method = RequestMethod.GET)
	public List<Category> getGreeting() {
		List<Category> result = categoryService.getAllCategory();

		return result;

	}

}
