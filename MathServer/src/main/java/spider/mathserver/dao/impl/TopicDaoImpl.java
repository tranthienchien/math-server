package spider.mathserver.dao.impl;

import org.springframework.stereotype.Repository;

import spider.mathserver.dao.TopicDao;
import spider.mathserver.model.Topic;

@Repository
public class TopicDaoImpl extends ObjectDaoImpl<Topic> implements TopicDao{

}
