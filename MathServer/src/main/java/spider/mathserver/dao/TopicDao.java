package spider.mathserver.dao;

import spider.mathserver.model.Topic;

public interface TopicDao extends ObjectDao<Topic> {

}
