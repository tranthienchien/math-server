package spider.mathserver.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import spider.mathserver.dao.CategoryDao;
import spider.mathserver.model.Category;
import spider.mathserver.service.CategoryService;

@Service
public class CateogryServiceImpl implements CategoryService {

	@Autowired
	CategoryDao categoryDao;

	@Transactional
	public List<Category> getAllCategory() {

		return categoryDao.getAll();
	}

}
