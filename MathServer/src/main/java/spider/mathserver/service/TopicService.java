package spider.mathserver.service;

import java.util.List;

import spider.mathserver.model.Topic;

public interface TopicService {
	
	public List<Topic> getAllTopicInCategory(Integer categoryID);
}
