package spider.mathserver.service;

import java.util.List;

import spider.mathserver.model.Category;

 

public interface CategoryService {
	List<Category> getAllCategory();
}
